import sys
from xml.dom import minidom
import subprocess
import os
from distutils.dir_util import copy_tree
from distutils.dir_util import remove_tree
from distutils.dir_util import mkpath
from shutil import copyfile
from pathlib import Path

import time

### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
### не забудь про то, что надо в res/integers.xml добавить версию gplay 
### <integer name="google_play_services_version">12451000</integer>
### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def remove_clear_traffic_constrains(path):
    xmldoc = minidom.parse(path)
    itemlist = xmldoc.getElementsByTagName("application")

    for item in itemlist:
        if item.hasAttribute("android:networkSecurityConfig"):
            item.removeAttribute("android:networkSecurityConfig")

        if not item.hasAttribute("android:usesCleartextTraffic"):
            item.setAttribute("android:usesCleartextTraffic", "true")

    f = open(path, "w")
    xmldoc.writexml(f)
    f.close()

def add_to_xml(xml_path, var_name, value, element_type, attr_name):
    xmldoc = minidom.parse(xml_path)
    ints = xmldoc.getElementsByTagName("resources")[0]
    var_list = []
    for i in ints.getElementsByTagName("integer"):
        var_list.append(i.getAttribute("name"))
    if var_name in var_list:
        print("There is a such varialable in this list")
        return -1
    else:
        v = xmldoc.createElement(element_type)
        v.setAttribute(attr_name, var_name)
        v_value = xmldoc.createTextNode(value)
        v.appendChild(v_value)
        xmldoc.childNodes[0].appendChild(v)

    print(xmldoc)
    with open(xml_path, "w") as xml_file:
        xmldoc.writexml(xml_file)

def get_elements_list(values, tag):
    var_list = []
    for i in values.getElementsByTagName(tag):
        element = (i.getAttribute("name"), i.getAttribute("type"))
        var_list.append(element)
    return var_list

def get_sub_styles_elements_list(values, tag):
    var_list = []
    for i in values.getElementsByTagName(tag):
        #print(i.getAttribute("name"))
        element = i.getAttribute("name")
        var_list.append(element)
    return var_list

def merge_xml(xml_path1, xml_path2):
    xmldoc1 = minidom.parse(xml_path1)
    xmldoc2 = minidom.parse(xml_path2)
    values1 = xmldoc1.getElementsByTagName("resources")[0]
    values2 = xmldoc2.getElementsByTagName("resources")[0]
    var_list1 = []
    var_list2 = []

    var_list1 = get_elements_list(values1, "item")
    var_list2 = get_elements_list(values2, "item")

    for elem in var_list2:
        if not(elem in var_list1):
            print(elem)
            v = xmldoc1.createElement("item")
            v.setAttribute("type", elem[1])
            v.setAttribute("name", elem[0])
            v_value = xmldoc1.createTextNode("False")
            v.appendChild(v_value)
            xmldoc1.childNodes[0].appendChild(v)

    with open(xml_path1, "w") as xml_file:
        xmldoc1.writexml(xml_file)

def merge_resources(res1_path, res2_path):
    files = os.listdir(res1_path)
    for i in files:
        merge_xml(res1_path, res2_path)
    pass    

# def merge_manifest(path_src, path_dest):
#     mainfest1 = open(path_src+"/AndroidManifest.xml", "r").read()  # read source manifest 
#     service1 = mainfest1[(mainfest1.find("<receiver")):mainfest1.find("</application")] # copy services and activities
#     permission1=mainfest1[mainfest1.find("<uses-permission"):mainfest1.find("<application")]# copy permissions
#     mainfest2 = open(path_dest+"/AndroidManifest.xml", "r").read() # read dest manifest
#     new_mainfest2 = mainfest2[0:mainfest2.find("<application")] +permission1+ mainfest2[mainfest2.find("<application"):mainfest2.find("</application")] + service1 + mainfest2[mainfest2.find("</application>"):mainfest2.find("</manifest>")+len("</manifest>")]
#     new_mainfest = open(path_dest+"/AndroidManifest.xml", "w")
#     new_mainfest.write(new_mainfest2)
#     new_mainfest.close()
    
def merge_manifest(xml_path1, xml_path2):

    manifest1 = minidom.parse(xml_path1)
    manifest2 = minidom.parse(xml_path2)

    values1 = manifest1.getElementsByTagName("manifest")[0] 

    container1 = manifest2.getElementsByTagName("manifest")[0].getElementsByTagName("application")[0]

    activities = values1.getElementsByTagName("activity")
    permission = values1.getElementsByTagName("uses-permission")
    services = values1.getElementsByTagName("service")
    metas = values1.getElementsByTagName("meta-data")
    receivers = values1.getElementsByTagName("receiver")

    for a in activities:
        container1.appendChild(a)
    for s in services:
        container1.appendChild(s)
    for m in metas:
        container1.appendChild(m)
    for r in receivers:
        container1.appendChild(r)
    for p in permission:    
        manifest2.childNodes[0].appendChild(p)
    
    with open(xml_path2, "w") as xml_file:
        manifest2.writexml(xml_file) 
    
def merge_styles(xml_path1, xml_path2):
    
    xmldoc1 = minidom.parse(xml_path1)
    xmldoc2 = minidom.parse(xml_path2)
    values1 = xmldoc1.getElementsByTagName("resources")[0]
    values2 = xmldoc2.getElementsByTagName("resources")[0]
    var_list1 = []
    var_list2 = []

    var_list1 = get_sub_styles_elements_list(values1, "style")
    var_list2 = get_sub_styles_elements_list(values2, "style")

    for i in var_list2:
        if not(i in var_list1):
            print(i)
            pass

    # print(var_list1)
    # print(var_list2)
    
    
if __name__ == "__main__":
    script_name = sys.argv[0]
    path_to_apk = sys.argv[1]
    
    apk_file_name = os.path.basename(path_to_apk)
    file_name = apk_file_name[:len(apk_file_name) - 4]

    if not(file_name in os.listdir("../tmps")):
        subprocess.call("java -jar ../tools/apktool_2.4.1.jar d ../apk_to_modify/{} -o ../tmps/{}".format(path_to_apk, file_name))

    source_path = "../tmps/{}".format(file_name)
    smali_folder = source_path + "/smali/"
    smali2_folder = source_path + "/smali_classes2/"

    merge_manifest("./AndroidManifest.xml", source_path + "/AndroidManifest.xml")

    merge_styles(source_path + "/res/values/styles.xml", "styles.xml")

    merge_xml(source_path + "/res/values/ids.xml", "ids.xml")

    add_to_xml(source_path + "/res/values/integers.xml", "google_play_services_version", "12451000", "integer", "name")

    copy_tree("../files_to_insert/smali", smali_folder)
    copy_tree("../files_to_insert/smali2", smali_folder)
    
    print("Building apk...")
    time.sleep(5)
    subprocess.call("java -jar ../tools/apktool_2.4.1.jar b ../tmps/{} -o new_test.apk".format(file_name))

    print("Singing apk...")
    time.sleep(5)
    subprocess.call("java -jar ../tools/apksigner.jar sign --ks-pass pass:12345678 --ks ../tools/inject.jks ./new_test.apk")